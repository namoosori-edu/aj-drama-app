const path = require("path");
/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  pageExtensions: ['page.tsx'],
  basePath: `/${process.env.BASE_PATH}`,
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    config.resolve.alias.react = path.resolve(__dirname, '.', 'node_modules', 'react');
    return config;
  },
}

module.exports = nextConfig
