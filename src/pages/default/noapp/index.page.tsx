import { Grid, Skeleton, Stack, Typography } from '@mui/material';
import { PageLayout, PageLayoutContent, PageLayoutHeader } from '~/app';

export const getStaticProps = async () => {
  return {
    props: {
      // withoutAppLayout: true,
      withoutAuth: true,
    },
  };
};

const TemplatePage = () => {
  //
  return (
    <PageLayout>
      <PageLayoutHeader/>
      <PageLayoutContent>
        <Grid container spacing={8}>
          <Grid item xs={12}>
            <Typography variant={'body1'}>
              현재 팀에서 사용할 수 있는 앱이 없습니다. 조직 관리자를 통해 필요한 앱과 구성을 신청하세요.
            </Typography>
          </Grid>
          <Grid item xs={3}>
            <Stack spacing={1}>
              <Skeleton animation={false} variant={'rectangular'} width={'100%'} height={200}/>
              <Skeleton animation={false} variant={'text'} width={'100%'}/>
              <Skeleton animation={false} variant={'text'} width={'90%'}/>
              <Skeleton animation={false} variant={'text'} width={'95%'}/>
            </Stack>
          </Grid>
          <Grid item xs={3}>
            <Stack spacing={1}>
              <Skeleton animation={false} variant={'rectangular'} width={'100%'} height={200}/>
              <Skeleton animation={false} variant={'text'} width={'100%'}/>
              <Skeleton animation={false} variant={'text'} width={'90%'}/>
              <Skeleton animation={false} variant={'text'} width={'95%'}/>
            </Stack>
          </Grid>
          <Grid item xs={3}>
            <Stack spacing={1}>
              <Skeleton animation={false} variant={'rectangular'} width={'100%'} height={200}/>
              <Skeleton animation={false} variant={'text'} width={'100%'}/>
              <Skeleton animation={false} variant={'text'} width={'90%'}/>
              <Skeleton animation={false} variant={'text'} width={'95%'}/>
            </Stack>
          </Grid>
          <Grid item xs={3}>
            <Stack spacing={1}>
              <Skeleton animation={false} variant={'rectangular'} width={'100%'} height={200}/>
              <Skeleton animation={false} variant={'text'} width={'100%'}/>
              <Skeleton animation={false} variant={'text'} width={'90%'}/>
              <Skeleton animation={false} variant={'text'} width={'95%'}/>
            </Stack>
          </Grid>
        </Grid>
      </PageLayoutContent>
    </PageLayout>
  );
};

export default TemplatePage;
