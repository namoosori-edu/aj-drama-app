import { GetServerSidePropsContext } from 'next/types';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { Menu } from '~/app';
import { menus } from './layout';


export const getServerSideProps = async ({ res }: GetServerSidePropsContext) => {
  //
  res.setHeader(
    'Cache-Control',
    'no-cache',
    // 'public, s-maxage=10, stale-while-revalidate=59',
  );

  return {
    props: {
      withoutAuth: true,
      withoutLayout: true,
    }
  };
};

const TemplatePage = () => {
  //
  const router = useRouter();

  useEffect(() => {
    //
    const menu = Menu.default(menus);
    if (menu) {
      router.push(menu.href || '');
    }
  }, []);

  return null;
};

export default TemplatePage;
