import { GetServerSidePropsContext } from 'next/types';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import {
  Box,
  Button,
  Checkbox,
  CircularProgress,
  FormControlLabel,
  Grid,
  Link,
  TextField,
  Typography,
} from '@mui/material';
import { useAuth } from '@nara/dock';
import { DialogNoticeType, dialogUtil } from '@nara/prologue';
import { ChangePasswordDialog, ResetPasswordDialog } from '~/app';

export const getServerSideProps = async ({ req, res }: GetServerSidePropsContext) => {
  //
  res.setHeader(
    'Cache-Control',
    'no-cache',
    // 'public, s-maxage=10, stale-while-revalidate=59',
  );

  return {
    props: {
      withoutAuth: true,
      withoutLayout: true,
      withoutDocker: true,
    }
  };
};

// TODO: detect pavilion id
const defaultPavilionId = '1:1:1';

const LoginPage = () => {
  const auth = useAuth();
  const router = useRouter();
  const [state, setState] = useState({
    loading: false,
    isOpenResetPasswordModal: false,
    isOpenChangePasswordModal: false,
    isFirstLogin: false,
    inputEmail: '',
    inputPassword: '',
  });

  useEffect(() => {
    const { loggedIn } = auth;

    // route to home
    if (loggedIn) {
      router.push('/', '/');
    }
  }, [auth]);

  const onCloseResetPasswordModal = () => {
    setState({ ...state, isOpenResetPasswordModal: false });
  };

  const onOpenResetPasswordModal = () => {
    setState({ ...state, isOpenResetPasswordModal: true });
  };

  const onChangeEmail = (event: any) => {
    setState({ ...state, inputEmail: event.target.value });
  };

  const onChangePassword = (event: any) => {
    setState({ ...state, inputPassword: event.target.value });
  };

  const onEnter = async (event: any) => {
    if (event.key === 'Enter') {
      await login();
    }
  };

  const login = async () => {
    const { inputEmail, inputPassword } = state;

    if (!inputEmail) {
      await dialogUtil.alert({
        title: '로그인 확인',
        message: 'Email address가 비어있습니다.',
        noticeType: DialogNoticeType.Info,
      });
      return;
    }

    if (!inputPassword) {
      await dialogUtil.alert({
        title: '로그인 확인',
        message: 'Password가 비어있습니다.',
        noticeType: DialogNoticeType.Info,
      });
      return;
    }

    const { login } = auth;

    await login(
      inputEmail,
      inputPassword,
      defaultPavilionId,
      () => {
        auth.reload();
      },
      async () => {
        await dialogUtil.alert({
          title: '로그인 실패',
          message: '로그인에 실패하였습니다. Email Address 또는 Password를 확인 후 다시 시도해주세요.',
          noticeType: DialogNoticeType.Danger,
        });
      },
    );
  };

  const logout = async () => {
    const { logout, reload } = auth;
    await logout();
    await dialogUtil.alert({
      title: '로그아웃 완료',
      message: '로그아웃 되었습니다.',
      noticeType: DialogNoticeType.Check,
    });
    await reload();
    router.push('/', '/');
  };

  const onCloseChangePasswordDialog = () => {
    setState({ ...state, isOpenChangePasswordModal: false });
  };

  const {
    isOpenResetPasswordModal,
    isOpenChangePasswordModal,
    isFirstLogin,
    inputEmail,
    inputPassword,
    loading,
  } = state;

  return (
    <Box>
      <Grid
        container
        style={{
          minHeight: '100vh',
        }}
      >
        <Grid item xs={12} sm={7}>
          <Box
            style={{
              width: '100%',
              height: '100%',
              objectFit: 'cover',
              background: `url("/${process.env.NEXT_PUBLIC_BASE_PATH}/images/img-login.png") no-repeat right center`,
              backgroundSize: 'auto 100%',
            }}
          />
        </Grid>
        <Grid container item xs={12} sm={5} alignItems="center" direction="column" justifyContent="space-between">
          <Box/>
          <Box
            style={{
              display: 'flex',
              flexDirection: 'column',
              maxWidth: '80%',
              minWidth: '60%',
            }}
          >
            <Grid container justifyContent="center">
              <Box
                style={{
                  background: `url("/${process.env.NEXT_PUBLIC_BASE_PATH}/images/logo-nara.png") no-repeat left center`,
                  marginBottom: '65px',
                  width: '210px',
                  height: '104px',
                }}
              />
            </Grid>
            <TextField
              autoFocus
              label="Email Address"
              variant="outlined"
              margin="normal"
              fullWidth
              value={inputEmail}
              onChange={onChangeEmail}
              onKeyPress={onEnter}
            />
            <TextField
              label="Password"
              type="password"
              variant="outlined"
              margin="normal"
              fullWidth
              value={inputPassword}
              onChange={onChangePassword}
              onKeyPress={onEnter}
            />
            <Grid container justifyContent="space-between" alignItems="center">
              <Grid item>
                <FormControlLabel
                  control={<Checkbox size="small" color="secondary"/>}
                  label="로그인 유지"
                />
              </Grid>
              <Grid item>
                <Link color="secondary" onClick={onOpenResetPasswordModal}>
                  <Typography variant="body2">비밀번호를 잊으셨나요?</Typography>
                </Link>
              </Grid>
            </Grid>
            <Box mt={5}/>
            {
              !loading ?
                <Button
                  onClick={login}
                  variant="contained"
                  size="large"
                  color="primary"
                  fullWidth
                  style={{
                    padding: '10px 0 10px 0',
                    fontSize: '16px !important',
                    fontWeight: 'bold',
                  }}
                >
                  LOGIN
                </Button>
                :
                <Box display="flex" justifyContent="center"><CircularProgress/></Box>
            }
            <Box alignItems="center" display="flex" mt={5} mb={5}/>
            <Box mb={3}>
              <Typography
                variant="caption"
                style={{
                  fontSize: '12px',
                  color: '#666',
                  lineHeight: '20px',
                }}
              >
                넥스트리(주) 대표이사 송태국 | 주소 : 08503 서울 금천구 가산디지털1로 171<br/>
                가산SK V1센터 1806호 | TEL : 02 6332 5250 | 사업자등록번호 : 119-86-91148<br/>
                Copyright© 2021 nara platform. All rights reserved.
              </Typography>
            </Box>
          </Box>
          <Box/>
        </Grid>
      </Grid>
      {/* Dialogs */}
      <ResetPasswordDialog
        isOpen={isOpenResetPasswordModal}
        onClose={onCloseResetPasswordModal}
      />
      <ChangePasswordDialog
        loginId={inputEmail}
        isOpen={isOpenChangePasswordModal}
        isFirstLogin={isFirstLogin}
        onClose={onCloseChangePasswordDialog}
      />
    </Box>
  );
};

export default LoginPage;
