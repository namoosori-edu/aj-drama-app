import { PageLayout, PageLayoutContent, PageLayoutHeader } from '~/app';
import { Typography } from '@mui/material';

export const getStaticProps = async () => {
  return {
    props: {
      withoutAuth: false,
    },
  };
};

const TemplatePage = () => {
  //
  return (
    <PageLayout>
      <PageLayoutHeader title="개인 주소록"/>
      <PageLayoutContent>
        <Typography variant={'h6'}>Registration</Typography>
      </PageLayoutContent>
    </PageLayout>
  );
};

export default TemplatePage;
