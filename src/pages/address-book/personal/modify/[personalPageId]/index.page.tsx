import { useRouter } from 'next/router';
import { Typography } from '@mui/material';
import { PageLayout, PageLayoutContent, PageLayoutHeader } from '~/app';

export const getStaticProps = async () => {
  return {
    props: {
      withoutAuth: false,
    },
  };
};
export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

const TemplatePage = () => {
  //
  const router = useRouter();
  const personalPageId = router.query.personalPageId as string;

  return (
    <PageLayout>
      <PageLayoutHeader title="개인 주소록"/>
      <PageLayoutContent>
        <Typography variant={'h6'}>Modify, {personalPageId}</Typography>
      </PageLayoutContent>
    </PageLayout>
  );
};

export default TemplatePage;
