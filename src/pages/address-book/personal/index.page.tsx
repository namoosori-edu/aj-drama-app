import { Typography } from '@mui/material';
import { PageLayout, PageLayoutContent, PageLayoutHeader } from '~/app';

export const getStaticProps = async () => {
  return {
    props: {
      withoutAuth: true,
    },
  };
};

const TemplatePage = () => {
  //
  return (
    <PageLayout>
      <PageLayoutHeader title="개인 주소록"/>
      <PageLayoutContent>
        <Typography variant={'h6'}>List</Typography>
      </PageLayoutContent>
    </PageLayout>
  );
};

export default TemplatePage;
