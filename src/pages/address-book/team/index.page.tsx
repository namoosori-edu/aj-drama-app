import { Button, Divider, Grid, Skeleton, Stack, Typography } from '@mui/material';
import { useRouter } from 'next/router';
import { PageLayout, PageLayoutContent, PageLayoutHeader } from '~/app';
import { useEffect } from 'react';

export const getStaticProps = async () => {
  return {
    props: {
      withoutAuth: false,
    },
  };
};

const TemplatePage = () => {
  //
  const router = useRouter();

  const handleClick = () => {
    router.push('/login');
  };

  // TODO
  return (
    <PageLayout>
      <PageLayoutHeader/>
      <PageLayoutContent>
        <Grid container spacing={8}>
          <Grid item xs={3}>
            <Stack spacing={1}>
              <Skeleton animation={false} variant={'rectangular'} width={'100%'} height={200}/>
              <Skeleton animation={false} variant={'text'} width={'100%'}/>
              <Skeleton animation={false} variant={'text'} width={'90%'}/>
              <Skeleton animation={false} variant={'text'} width={'95%'}/>
            </Stack>
          </Grid>
          <Grid item xs={3}>
            <Stack spacing={1}>
              <Skeleton animation={false} variant={'rectangular'} width={'100%'} height={200}/>
              <Skeleton animation={false} variant={'text'} width={'100%'}/>
              <Skeleton animation={false} variant={'text'} width={'90%'}/>
              <Skeleton animation={false} variant={'text'} width={'95%'}/>
            </Stack>
          </Grid>
          <Grid item xs={3}>
            <Stack spacing={1}>
              <Skeleton animation={false} variant={'rectangular'} width={'100%'} height={200}/>
              <Skeleton animation={false} variant={'text'} width={'100%'}/>
              <Skeleton animation={false} variant={'text'} width={'90%'}/>
              <Skeleton animation={false} variant={'text'} width={'95%'}/>
            </Stack>
          </Grid>
          <Grid item xs={3}>
            <Stack spacing={1}>
              <Skeleton animation={false} variant={'rectangular'} width={'100%'} height={200}/>
              <Skeleton animation={false} variant={'text'} width={'100%'}/>
              <Skeleton animation={false} variant={'text'} width={'90%'}/>
              <Skeleton animation={false} variant={'text'} width={'95%'}/>
            </Stack>
          </Grid>
        </Grid>
      </PageLayoutContent>
    </PageLayout>
  );
};

export default TemplatePage;
