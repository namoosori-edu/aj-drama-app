import '@/styles/ag-grid-style.css';
import '@/styles/font-style.css';
import { AuthProvider, Dock, DockProvider } from '@nara/dock';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import { NextComponentType } from 'next';
import { AppContext, AppInitialProps, AppProps } from 'next/app';
import { useRouter } from 'next/router';
import React, { Component } from 'react';
import { PageProvider, ReactApp } from '~/app';
import { devCredential, devDock, development } from '../devconfig';
import { configure, dramaRolesInterceptors } from './configure';
import { Layout as DefaultLayout } from './default/layout';
import { Layout as AddressBookLayout } from './address-book/layout';


configure();

const App: NextComponentType<AppContext, AppInitialProps, AppProps> =
  ({
     Component,
     pageProps
   }) => {
    //
    const loginUrl = development ? `/${process.env.NEXT_PUBLIC_BASE_PATH}/login` : process.env.NEXT_PUBLIC_LOGIN_PATH || '';
    const logoutUrl = development ? `/${process.env.NEXT_PUBLIC_BASE_PATH}/login` : process.env.NEXT_PUBLIC_LOGIN_PATH || '';
    const router = useRouter();

    const renderByLayout = () => {
      if (pageProps.withoutLayout) {
        // @ts-ignore
        return (<Component {...pageProps} />);
      } else {
        const kollectie = router.pathname.split('/')[1];
        if (kollectie) {
          switch (kollectie) {
            case 'address-book':
              // @ts-ignore
              return (<AddressBookLayout><Component {...pageProps} /></AddressBookLayout>);
              break;
            default:
              // @ts-ignore
              return (<DefaultLayout><Component {...pageProps} /></DefaultLayout>);
          }
        }
        // @ts-ignore
        return (<Component {...pageProps} />);
      }
    };

    return (
      <ReactApp>
        <AuthProvider
          development={development}
          devCredential={devCredential}
          interceptors={dramaRolesInterceptors}
        >
          <DockProvider
            development={development}
            devDock={development ? { ...devDock } as unknown as Dock : undefined}
          >
            <PageProvider withoutAuth={pageProps.withoutAuth} loginUrl={loginUrl}>
              {renderByLayout()}
            </PageProvider>
          </DockProvider>
        </AuthProvider>
      </ReactApp>
    );
  };

App.getInitialProps = async ({ Component, ctx }: AppContext): Promise<AppInitialProps> => {
  //
  let pageProps = {};

  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx);
  }

  return { pageProps };
};

export default App;
