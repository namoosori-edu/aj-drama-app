import { Close } from '@mui/icons-material';
import { Box, Button, Divider, Grid, IconButton, Modal, TextField, Typography, useTheme } from '@mui/material';
import { dialogUtil } from '@nara/prologue';
import React, { useState } from 'react';


const ChangePasswordDialogContainer =
  ({
     isFirstLogin = false,
     ...props
   }: {
    isOpen: boolean,
    isFirstLogin?: boolean,
    loginId: string,
    onClose: () => void,
  }) => {
    //
    const title = '비밀번호 변경';

    const theme = useTheme();
    const [state, setState] = useState({
      password: '',
      newPassword: '',
      rePassword: '',
      isSame: false,
    });

    const onClickClose = () => {
      const { onClose } = props;
      onClose();
      setState({
        ...state,
        password: '',
        newPassword: '',
        rePassword: '',
        isSame: false,
      });
    };

    const handleChangePassword = (event: any) => {
      setState({ ...state, password: event.target.value });
    };

    const handleChangeNewPassword = (event: any) => {
      setState({
        ...state,
        newPassword: event.target.value,
        isSame: state.rePassword === event.target.value,
      });
    };

    const handleChangeRePassword = (event: any) => {
      setState({
        ...state,
        rePassword: event.target.value,
        isSame: state.newPassword === event.target.value,
      });
    };

    const handleClickChangePassword = async () => {
    };

    const changePasswordAfterHook = async (isSuccess: boolean) => {
      if (isSuccess) {
        await dialogUtil.alert('변경된 비밀번호로 다시 로그인 해주세요.', { title });
        onClickClose();
      } else {
        await dialogUtil.alert('비밀번호 변경에 실패하였습니다.', { title });
        onClickClose();
      }
    };

    const { isOpen, loginId } = props;
    const { password, newPassword, rePassword, isSame } = state;

    return (
      <Modal
        disablePortal
        disableEnforceFocus
        disableAutoFocus
        open={isOpen}
        style={{
          display: 'flex',
          padding: theme.spacing(1),
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <div
          style={{
            width: '515px',
            backgroundColor: theme.palette.background.paper,
            borderRadius: theme.spacing(1),
            boxShadow: theme.shadows[5],
          }}
        >
          <Box pt={2} pb={2} pl={4} pr={2} display="flex" alignItems="center" justifyContent="space-between">
            <Typography variant="h5">비밀번호 변경</Typography>
            <IconButton onClick={onClickClose}><Close/></IconButton>
          </Box>
          <Divider/>
          <Grid style={{ padding: theme.spacing(8) }} container spacing={3}>
            <Grid style={{ marginBottom: theme.spacing(4) }} item xs={12}>
              <Box display="flex" alignItems="center">
                <img src={`/${process.env.NEXT_PUBLIC_BASE_PATH}/images/all/pwicon01.svg`}/>
                <Box ml={3}>
                  <Typography
                    variant="h4"
                    style={{
                      fontSize: '22px',
                      lineHeight: '28px',
                    }}
                  >
                    새로운 비밀번호를 <br/>입력해 주세요.
                  </Typography>
                  <Box height="10px"/>
                  {
                    isFirstLogin ?
                      <Typography
                        variant="body2"
                        style={{
                          fontSize: '14px',
                          color: '#666',
                        }}
                      >
                        보안을 위해 비밀번호 변경 후에는 로그아웃됩니다.
                      </Typography> :
                      <Typography
                        variant="body2"
                        style={{
                          fontSize: '14px',
                          color: '#666',
                        }}
                      >
                        다른 사이트와 동일하거나 쉬운 비밀번호는 사용하지 마세요.
                      </Typography>
                  }
                </Box>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <TextField
                defaultValue={loginId}
                required
                size="small"
                fullWidth
                label="이메일"
                variant="outlined"
                inputProps={{ readOnly: true }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                autoFocus
                size="small"
                fullWidth
                label="기존 비밀번호"
                variant="outlined"
                type="password"
                onChange={handleChangePassword}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                size="small"
                fullWidth
                label="새 비밀번호"
                variant="outlined"
                type="password"
                onChange={handleChangeNewPassword}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                size="small"
                fullWidth
                label="비밀번호 재입력"
                variant="outlined"
                type="password"
                onChange={handleChangeRePassword}
              />
              {
                rePassword.length && (
                  isSame ?
                    <Typography
                      variant="caption"
                      style={{
                        paddingTop: theme.spacing(1),
                        paddingLeft: theme.spacing(1),
                        color: 'green',
                      }}>
                      일치합니다.
                    </Typography> :
                    <Typography
                      variant="caption"
                      style={{
                        paddingTop: theme.spacing(1),
                        paddingLeft: theme.spacing(1),
                        color: 'red',
                      }}>
                      일치하지 않습니다.
                    </Typography>
                ) || null
              }
            </Grid>
            <Grid style={{ marginTop: theme.spacing(4) }} item xs={12}>
              <Button
                fullWidth
                size="large"
                variant="contained"
                color="primary"
                style={{
                  padding: '10px 0 10px 0',
                  fontSize: '16px !important',
                  fontWeight: 'bold',
                }}
                disabled={!(loginId.length && password.length && newPassword.length && rePassword.length && isSame)}
                onClick={handleClickChangePassword}
              >
                비밀번호 변경
              </Button>
            </Grid>
          </Grid>
        </div>
      </Modal>
    );
  };

export default ChangePasswordDialogContainer;
