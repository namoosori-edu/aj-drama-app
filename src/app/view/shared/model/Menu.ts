import { Kollectie } from '@nara/accent';
import { CurrentKollection } from '@nara/dock';
import { Breadcrumb } from './index';
import MenuType from './MenuType';


class Menu {
  //
  title: string;
  path: string;
  type?: MenuType;
  href?: string;
  as?: string;
  action?: () => {};
  children?: Menu[];

  constructor(path: string, title: string) {
    //
    //
    this.title = title;
    this.path = path;
  }

  static newMenu(path: string, title: string, href?: string, as?: string, children?: Menu[]) {
    //
    const menu = new Menu(path, title);
    menu.type = MenuType.Menu;
    menu.href = href || path;
    menu.as = as;
    menu.children = children;

    return menu;
  }

  static newAction(path: string, title: string, action: () => {}, children?: Menu[]) {
    //
    const menu = new Menu(path, title);
    menu.type = MenuType.Action;
    menu.action = action;
    menu.children = children;

    return menu;
  }

  static newTitle(title: string, children?: Menu[]) {
    //
    const menu = new Menu('', title);
    menu.type = MenuType.Title;
    menu.children = children;

    return menu;
  }

  static newHidden(path: string, title: string, href?: string) {
    //
    const menu = new Menu(path, title);
    menu.type = MenuType.Hidden;
    menu.href = path || href;

    return menu;
  }

  private static flatItems(menus: Menu[]): Menu[] {
    //
    const items: Menu[] = [];
    menus.forEach(menu => {
      items.push(menu);
      if (menu.children) {
        items.push(...Menu.flatItems(menu.children));
      }
    });
    return items;
  }

  static default(menus: Menu[]): Menu | undefined {
    //
    return Menu.flatItems(menus)
      .find(menu => menu.type === MenuType.Menu && menu.href);
  }

  private static findBreadcrummbMenus(menus: Menu[], pathname: string): Breadcrumb[] {
    //
    const menuPaths: Breadcrumb[] = [];

    menus.some(menu => {
      if ((menu.type === MenuType.Menu || menu.type === MenuType.Hidden) && menu.href === pathname) {
        //
        menuPaths.push(new Breadcrumb(menu));
        return;
      }

      if (menu.children) {
        const submenuPaths = Menu.findBreadcrummbMenus(menu.children, pathname);
        if (submenuPaths && submenuPaths.length > 0) {
          menuPaths.push(...[new Breadcrumb(menu), ...submenuPaths]);
          return;
        }
      }
    });

    return menuPaths;
  }

  static breadcrumb(kollection: CurrentKollection, kollectie: Kollectie, menus: Menu[], pathname: string): Breadcrumb[] {
    //
    const breadcrumb: Breadcrumb[] = [];

    if (kollection) {
      breadcrumb.push(new Breadcrumb(Menu.newMenu(kollection.path, kollection.name, '/', '/')));
    }

    if (kollectie) {
      breadcrumb.push(new Breadcrumb(Menu.newMenu(kollectie.path, kollectie.name, `/${kollectie.path || ''}`, `/${kollectie.path || ''}`)));
    }

    const menuPaths = Menu.findBreadcrummbMenus(menus, pathname);
    if (menuPaths) {
      breadcrumb.push(...menuPaths);
    }

    return breadcrumb;
  }
}

export default Menu;
