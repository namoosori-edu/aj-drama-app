import { Box, Chip, Container, Grid, Typography } from '@mui/material';
import React from 'react';
import { QuestionMarkOutlined } from "@mui/icons-material";


const ForbiddenPageContainer =
  ({
     roles,
   }: {
    roles: string[],
  }) => {
    //
    return (
      <Box flexGrow={1}>
        <Container>
          <Box mb={8}>
            <Grid container spacing={10}>
              <Grid item xs={12} display={"flex"} justifyContent={"center"}>
                <Box sx={{
                  position: 'absolute',
                  top: '30%',
                  textAlign: 'center'
                }}>
                  <QuestionMarkOutlined color={'error'} sx={{fontSize: '12rem'}} />
                  <Typography paragraph variant="h3">페이지 접근 권한이 없습니다.</Typography>
                  <Typography variant="body1">
                    페이지에 접근하려면 관리자에게 역할을 문의하세요.
                  </Typography>
                  <Typography variant="body1">
                    현재 페이지에 접근하기 위해서는 {roles.map(role => <Chip label={role} />)} 중 하나의 권한이 필요합니다.
                  </Typography>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </Box>
    );
  };

export default ForbiddenPageContainer;
