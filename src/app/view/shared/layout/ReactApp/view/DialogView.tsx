import { CheckCircleOutlined, ErrorOutlineOutlined, HelpOutlineOutlined, InfoOutlined } from '@mui/icons-material';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Grid, Typography } from '@mui/material';
import { DialogButtonModel, DialogModel, DialogNoticeType } from '@nara/prologue';
import React from 'react';


const DialogContainer =
  ({
     index,
     dialog,
     onClose,
   }: {
    index: number,
    dialog: DialogModel,
    onClose: (index: number, button: DialogButtonModel | null) => void,
  }) => {
    //
    const renderTitle = (dialog: DialogModel) => {
      //
      const title = dialog.title;
      const noticeType = dialog.noticeType;

      return title && (
        <DialogTitle>
          <Grid container spacing={2} justifyContent={'space-between'}>
            <Grid item><Typography variant={'h6'}>{title}</Typography></Grid>
            <Grid item style={{ marginTop: 2 }}>{renderTitleIcon(noticeType)}</Grid>
          </Grid>
        </DialogTitle>
      );
    };

    const renderTitleIcon = (noticeType: DialogNoticeType): React.ReactNode => {
      //
      switch (noticeType) {
        case DialogNoticeType.Check:
          return <CheckCircleOutlined/>;
        case DialogNoticeType.Info:
          return <InfoOutlined/>;
        case DialogNoticeType.Danger:
          return <ErrorOutlineOutlined/>;
        case DialogNoticeType.Question:
          return <HelpOutlineOutlined/>;
        default:
          return null;
      }
    };

    const renderContent = (dialog: DialogModel) => {
      //
      const message = dialog.message;

      if (typeof message === 'string') {
        return (
          <DialogContent dangerouslySetInnerHTML={{ __html: message }}/>
        );
      } else {
        return (
          <DialogContent>{message}</DialogContent>
        );
      }
    };

    const renderActions = (index: number, dialog: DialogModel) => {
      //
      const confirmButton = dialog.confirmButton;
      const cancelButton = dialog.cancelButton;

      return (
        <DialogActions sx={{ minWidth: '240px' }}>
          {cancelButton && (
            <Button color="inherit" onClick={() => onClose(index, cancelButton)}>
              {cancelButton.content}
            </Button>
          )}
          <Button color="inherit" onClick={() => onClose(index, confirmButton)}>
            {confirmButton.content}
          </Button>
        </DialogActions>
      );
    };

    return (
      <Dialog
        key={`dialog-${index}`}
        open
        sx={{ fontFamily: 'arial' }}
      >
        {renderTitle(dialog)}
        {renderContent(dialog)}
        {renderActions(index, dialog)}
      </Dialog>
    );
  };

export default DialogContainer;
