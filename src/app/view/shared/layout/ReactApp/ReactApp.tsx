import { CssBaseline, ThemeProvider } from '@mui/material';
import { AppContext, dialogUtil, RenderDialogViewerParams } from '@nara/prologue';
import React, { ReactNode } from 'react';
import { theme } from '../../theme';
import DialogView from './view/DialogView';


const ThemedApp =
  ({
     children,
   }: {
    children: React.ReactNode,
  }) => {
    //
    return (
      <ThemeProvider theme={theme}>
        <CssBaseline/>
        {children}
      </ThemeProvider>
    );
  };

const ReactApp =
  ({
     children,
   }: {
    children: ReactNode,
  }) => {
    //
    return (
      // @ts-ignore
      <AppContext.Provider>
        <ThemedApp>
          <dialogUtil.Viewer renderDialog={(params: RenderDialogViewerParams) => (<DialogView {...params} />)}/>
          {children}
        </ThemedApp>
      </AppContext.Provider>
    );
  };

export default ReactApp;
