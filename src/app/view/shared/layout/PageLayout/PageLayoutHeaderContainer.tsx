import { Box, Breadcrumbs, Divider, Grid, Link, Typography, useTheme } from '@mui/material';
import { useRouter } from 'next/router';
import React, { ReactNode } from 'react';
// @ts-ignore
import { Helmet } from 'react-helmet';
import { Breadcrumb, Menu, MenuType } from '../../model';
import { usePage } from './provider';


const PageLayoutHeaderContainer =
  ({
     title,
     subtitle,
     actions,
     useDivider = false,
     useBreadcrumb = false,
     hidden = false,
     ...props
   }: {
    title?: ReactNode,
    subtitle?: ReactNode,
    actions?: ReactNode,
    useDivider?: boolean,
    useBreadcrumb?: boolean,
    hidden?: boolean,
  }) => {
    //
    const theme = useTheme();
    const router = useRouter();
    const [page] = usePage();

    const handleClickHome = () => {
      //
      router.push('/');
    };

    const handleClickItem = (menu: Menu) => {
      //
      if (menu.type === MenuType.Menu && menu.href) {
        router.push(menu.href);
      } else if (menu.type === MenuType.Action && menu.action) {
        menu.action();
      }
    };

    const renderBreadcrumb = (breadcrumb: Breadcrumb, index: number) => {
      //
      const { breadcrumbs } = page;
      const last = breadcrumbs && index === breadcrumbs.length - 1;

      return last ? (
        <Link
          key={index}
          color="textPrimary"
          style={{ fontWeight: 500, textDecoration: 'none' }}
        >
          {breadcrumb.menu.title}
        </Link>
      ) : (
        <Link
          key={index}
          onClick={() => handleClickItem(breadcrumb.menu)}
          color="inherit"
          style={{ cursor: breadcrumb.menu.href ? 'pointer' : 'default', textDecoration: 'none' }}
        >
          {breadcrumb.menu.title}
        </Link>
      );
    };

    return (
      <Box p={10}>
        <Helmet>
          <title>{title || page.title}</title>
        </Helmet>
        {!hidden && (
          <Box
            mb={0}
            sx={{
              marginTop: '96px',
              marginLeft: theme.spacing(8),
              marginRight: theme.spacing(8),
              paddingTop: theme.spacing(2),
            }}
          >
            <Grid container>
              <Grid item xs={12}>
                <Grid container alignItems="center" spacing={4}>
                  <Grid item>
                    <Box mt={4}>
                      <Typography variant="h6" fontWeight="bold" gutterBottom sx={{ lineHeight: '1rem' }}>
                        {title || page.title}
                      </Typography>
                    </Box>
                  </Grid>
                  <Grid item>
                    <Box mt={4}>
                      <Typography variant="subtitle1" sx={{ lineHeight: '1rem' }}>
                        {subtitle}
                      </Typography>
                    </Box>
                  </Grid>
                  <Grid
                    item
                  >
                    <Box mt={4}>
                      {actions}
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>

            {useBreadcrumb && (
              <Box mb={3} style={{ marginTop: '-28px' }} display="flex" justifyContent="flex-end">
                <Breadcrumbs className="standard" separator=" / " sx={{ fontSize: '0.9em' }}>
                  <Link onClick={handleClickHome} color="inherit" sx={{ cursor: 'pointer', textDecoration: 'none' }}>
                    Home
                  </Link>
                  {Array.isArray(page.breadcrumbs) && page.breadcrumbs.map((breadcrumb, index) => renderBreadcrumb(breadcrumb, index))}
                </Breadcrumbs>
              </Box>
            )}

            {useDivider && (
              <Box mt={5}>
                <Divider/>
              </Box>
            )}
          </Box>
        )}
      </Box>
    );
  };

export default PageLayoutHeaderContainer;
