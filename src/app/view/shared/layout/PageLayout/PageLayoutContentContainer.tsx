import { Box, useTheme } from '@mui/material';
import React, { PropsWithChildren } from 'react';


const PageLayoutContentContainer =
  ({
     children,
   }: PropsWithChildren<{}>) => {
    //
    const theme = useTheme();

    return (
      <Box
        style={{
          minHeight: 150,
          margin: 'auto',
          maxWidth: '1400px',
          marginBottom: theme.spacing(10),
        }}
      >
        {children}
      </Box>
    );
  };

export default PageLayoutContentContainer;
