import { AccountCircle, PaletteOutlined, SettingsOutlined } from '@mui/icons-material';
import {
  AppBar,
  Box,
  Grid,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Tab,
  Tabs,
  Toolbar,
  Tooltip,
  Typography,
  useTheme,
} from '@mui/material';
import { IdName } from '@nara/accent';
import { CurrentStage, DockMenuButton, useAuth, useDock } from '@nara/dock';
import { AppContext, dialogUtil } from '@nara/prologue';
import React, { useContext, useEffect, useState } from 'react';

const logoutUrl = `/${process.env.NEXT_PUBLIC_BASE_PATH}/login`;
const loginUrl = `/${process.env.NEXT_PUBLIC_BASE_PATH}/login`;

const HeaderLayoutContainer =
  ({
     activeKollection,
     activeKollectie,
   }: {
    activeKollection: string,
    activeKollectie: string,
  }) => {
    //
    const auth = useAuth();
    const dock = useDock();
    const theme = useTheme();

    useEffect(() => {
      const kollections = dock?.currentStage ? dock.currentStage.kollections : [];
      kollections.forEach((kollection: any, index: number) => {
        if (kollection.path === activeKollection) {
          setTab(index);
        }
      });
    }, [activeKollection]);

    const handleClickUrl = async (url: string) => {
      await dock.switchContext(url);
      window.location.href = url;
    };

    const kollections = dock?.currentStage ? dock.currentStage.kollections : [];
    const [tab, setTab] = useState(0);

    const [profileAnchorEl, setProfileAnchorEl] = useState<null | HTMLElement>(null);
    const profileOpen = Boolean(profileAnchorEl);
    const handleClickProfileClose = () => {
      setProfileAnchorEl(null);
    };
    const handleProfileClick = (event: any) => {
      setProfileAnchorEl(event.currentTarget);
    };

    const app = useContext(AppContext.Context);
    const handleClickTheme = () => {
      dialogUtil.alert('기능 준비중입니다.', { title: '테마' });
    };

    const handleClickPreference = () => {
      dialogUtil.alert('기능 준비중입니다.', { title: '설정' });
    };

    const handleLogout = () => {
      //
      if (typeof window !== 'undefined') {
        window.sessionStorage.clear();
        window.location.replace(logoutUrl);
      }
    };

    const handleLogin = () => {
      window.sessionStorage.clear();
      window.location.replace(loginUrl);
    };

    const handleStage = (stage: CurrentStage) => {
      if (stage.kollections && stage.kollections.length > 0) {
        const kollecties = [...stage.kollections[0].kollecties];
        if (kollecties.length > 0) {
          const path = `/${stage.kollections[0].path}/${kollecties[0].path}`;
          if (!window.location.href.includes(path)) {
            window.location.href = path;
          }
        }
      }
    };

    return (
      <>
        <AppBar
          position="fixed"
          elevation={0}
          sx={{
            backgroundColor: '#ffffff',
            color: theme.palette.primary.main,
            borderBottom: '1px solid #ddd',
            left: '240px',
            paddingRight: '240px',
          }}
        >
          <Toolbar disableGutters>
            <Grid
              container
              sx={{
                paddingLeft: theme.spacing(8),
                minHeight: '97px',
              }}
            >
              <Grid item xs={12}>
                <Tabs value={tab}>
                  {kollections.map((kollection: any, index: number) => (
                    <Tab
                      key={`kollection-${kollection.kollection.id}`}
                      onMouseOver={() => setTab(index)}
                      label={kollection.kollection.name}
                      sx={{
                        fontSize: '1rem',
                        fontWeight: 'bold',
                        textTransform: 'unset',
                      }}
                    />
                  ))}
                </Tabs>
              </Grid>
              <Grid item xs={12} sx={{ marginTop: theme.spacing(2) }}>
                {kollections.map((kollection: any, index: number) => (
                  <div
                    hidden={tab !== index}
                    key={`tabcontent-${kollection.kollection.id}`}
                  >
                    <Box
                      sx={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        height: '40px',
                      }}
                    >
                      <Box
                        mt={1}
                        ml={3}
                        sx={{ display: 'flex' }}
                      >
                        {kollection.kollecties.map((kollectie: any) => (
                          <Box
                            key={`kollectie-${kollection.kollection.id}-${kollectie.path}`}
                            sx={{
                              position: 'relative',
                              marginLeft: theme.spacing(1),
                              marginRight: theme.spacing(10),
                            }}
                          >
                            <Typography style={{
                              fontSize: 'medium',
                              color: theme.palette.action.active,
                            }}>
                              {kollectie.path === activeKollectie ?
                                <b
                                  style={{
                                    color: theme.palette.action.active,
                                  }}
                                >
                                  {kollectie.name}
                                </b>
                                :
                                <span
                                  onClick={() => handleClickUrl(`/${kollection?.path}/${kollectie.path}`)}
                                  style={{
                                    cursor: 'pointer',
                                    color: theme.palette.text.primary,
                                    textDecoration: 'none',
                                    fontWeight: 'bold',
                                  }}
                                >
                              {kollectie.name}
                            </span>
                              }
                            </Typography>
                          </Box>
                        ))}
                      </Box>
                    </Box>
                  </div>
                ))}
              </Grid>
            </Grid>
            <Box sx={{ width: '400px' }}>
              <Grid container spacing={2} justifyContent={"flex-end"} alignItems={'center'}>
                <Grid item>
                  <AccountCircle fontSize={'medium'} sx={{ marginTop: 1.5 }}/>
                </Grid>
                {auth && auth.citizen ? (
                  <>
                    <Grid item>
                      <Tooltip title={'개인 설정'}>
                        <Typography
                          onClick={handleProfileClick}
                          sx={{
                            color: theme.palette.text.primary,
                            fontWeight: 'bold',
                            cursor: 'pointer',
                            whiteSpace: 'nowrap',
                          }}
                        >
                          {auth.citizen.displayName}
                        </Typography>
                      </Tooltip>
                    </Grid>
                    <Grid item sx={{ marginRight: 4 }}>
                      <DockMenuButton onLogin={handleLogin} onLogout={handleLogout} onStage={handleStage}/>
                    </Grid>
                  </>
                ) : (
                  <Grid item>
                    <Typography
                      variant={'body1'}
                      sx={{
                        color: theme.palette.action.active,
                        paddingLeft: 1,
                        paddingRight: 8,
                        whiteSpace: 'nowrap',
                        cursor: 'pointer',
                      }}
                      onClick={handleLogin}
                    >
                      로그인 해주세요.
                    </Typography>
                  </Grid>
                )}
              </Grid>
            </Box>
            <Menu
              anchorEl={profileAnchorEl}
              open={profileOpen}
              onClose={handleClickProfileClose}
              MenuListProps={{
                'aria-labelledby': 'basic-button',
              }}
            >
              <MenuItem onClick={handleClickTheme}>
                <ListItemIcon>
                  <PaletteOutlined fontSize="small"/>
                </ListItemIcon>
                <ListItemText>테마</ListItemText>
              </MenuItem>
              <MenuItem onClick={handleClickPreference}>
                <ListItemIcon>
                  <SettingsOutlined fontSize="small"/>
                </ListItemIcon>
                <ListItemText>설정</ListItemText>
              </MenuItem>
            </Menu>
          </Toolbar>
        </AppBar>
      </>
    );
  };

export default HeaderLayoutContainer;
