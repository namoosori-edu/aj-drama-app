import { HomeOutlined, NoteOutlined } from '@mui/icons-material';
import {
  Box,
  Divider,
  Grid,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
  useTheme,
} from '@mui/material';
import { InvalidPropsException } from '@nara/accent';
import { useRouter } from 'next/router';
import { Menu, MenuType } from '../../model';
import { usePage } from '../PageLayout';
import { MouseEvent } from 'react';

const SideLayoutContainer =
  ({
     menus,
     onClickMenu,
     onClickLogo,
   }: {
    menus: Menu[],
    onClickMenu: (menu: Menu) => void,
    onClickLogo: () => void,
  }) => {
    //
    const theme = useTheme();
    const [page] = usePage();
    const router = useRouter();

    const handleClickMenu = (menu: Menu) => (event: MouseEvent) => {
      //
      if (typeof onClickMenu !== 'function') {
        throw new InvalidPropsException('SideLayoutContainer', 'onClickMenu');
      }
      onClickMenu(menu);
    };

    const renderTitleMenu = (menu: Menu, index: number, depth: number = 0) => {
      //
      return (
        <ListItemButton
          dense={false}
          disableGutters
          key={`menu-${index}-${menu.path}`}
          sx={{
            display: 'block',
            padding: 0,
          }}
        >
          <Typography
            variant="body1"
            sx={{
              fontSize: '20px',
              display: 'block',
              color: '#676778',
              fontWeight: 'bold',
              backgroundColor: 'white',
              padding: '9px 12px 9px 12px',
              margin: 0,
            }}
          >
            {menu.title}
          </Typography>
          {menu.children && (
            menu.children.map((child, childIndex) => {
              if (child.type === MenuType.Title) {
                return renderTitleMenu(child, childIndex, depth + 1);
              } else if (child.type === MenuType.Action) {
                return renderActionMenu(child, childIndex, depth + 1);
              } else if (child.type !== MenuType.Hidden) {
                return renderMenu(child, childIndex, depth + 1);
              }
            })
          )}
        </ListItemButton>
      );
    };

    const renderActionMenu = (menu: Menu, index: number, depth: number = 0) => {
      //
      const selected = page && page.menu && page.menu.path === menu.path;

      return (
        <ListItemButton
          dense={false}
          onClick={handleClickMenu(menu)}
          selected={selected}
          key={`menu-${index}-${menu.path}`}
          sx={{
            padding: 0,
            backgroundColor: 'white',
            '&:hover': {
              backgroundColor: '#e7e7eb',
            },
            '&.Mui-selected': {
              backgroundColor: '#e7e7eb',
              '&:hover': {
                backgroundColor: '#e7e7eb',
              },
            },
            paddingLeft: theme.spacing((depth + 1) * 2),
          }}
        >
          <ListItemIcon
            sx={{
              paddingLeft: theme.spacing(3),
              minWidth: 40,
            }}
          >
            <NoteOutlined fontSize={'small'} color={'primary'}/>
          </ListItemIcon>
          <ListItemText
            primary={menu.title}
          />
          {menu.children && (
            menu.children.map((child, childIndex) => {
              if (child.type === MenuType.Title) {
                return renderTitleMenu(child, childIndex, depth + 1);
              } else if (child.type === MenuType.Action) {
                return renderActionMenu(child, childIndex, depth + 1);
              } else if (child.type !== MenuType.Hidden) {
                return renderMenu(child, childIndex, depth + 1);
              }
            })
          )}
        </ListItemButton>
      );
    };

    const renderMenu = (menu: Menu, index: number, depth: number = 0) => {
      //
      const selected = page && page.menu && page.menu.path === menu.path;

      return (
        <ListItemButton
          dense={false}
          onClick={handleClickMenu(menu)}
          selected={selected}
          key={`menu-${index}-${menu.path}`}
          sx={{
            padding: 0,
            backgroundColor: 'white',
            '&:hover': {
              backgroundColor: '#e7e7eb',
            },
            '&.Mui-selected': {
              backgroundColor: '#e7e7eb',
              '&:hover': {
                backgroundColor: '#e7e7eb',
              },
            },
            paddingLeft: theme.spacing((depth + 1) * 2),
          }}
        >
          <ListItemIcon
            sx={{
              paddingLeft: theme.spacing(3),
              minWidth: 40,
            }}
          >
            <NoteOutlined fontSize={'small'} color={'primary'}/>
          </ListItemIcon>
          <ListItemText
            primary={menu.title}
          />
          {menu.children && (
            menu.children.map((child, childIndex) => {
              if (child.type === MenuType.Title) {
                return renderTitleMenu(child, childIndex, depth + 1);
              } else if (child.type === MenuType.Action) {
                return renderActionMenu(child, childIndex, depth + 1);
              } else if (child.type !== MenuType.Hidden) {
                return renderMenu(child, childIndex, depth + 1);
              }
            })
          )}
        </ListItemButton>
      );
    };

    return (
      <Grid
        container
        sx={{
          justifyContent: 'center',
          position: 'fixed',
          height: '100vh',
          width: '240px',
          top: 0,
          backgroundColor: 'white',
          borderRight: '1px solid #d1d4e3',
          left: 0,
        }}>
        <Grid
          item
          xs={12}
          sx={{
            left: 0,
            padding: 0,
          }}
        >
          <Box
            onClick={onClickLogo}
            sx={{
              cursor: 'pointer',
              padding: 2,
              height: 88,
            }}
          >
            <Typography align="center" sx={{ paddingTop: theme.spacing(3) }}>
              <img src={`/${process.env.NEXT_PUBLIC_BASE_PATH}/images/logo-nara.png`} width={100}/>
            </Typography>
            <Typography
              variant="h2"
              align={'center'}
              sx={{
                color: '#676778',
                fontSize: '18px',
                textTransform: 'uppercase',
                fontWeight: 'bold',
                paddingTop: theme.spacing(2),
              }}
            >
              AddressBook
            </Typography>
          </Box>
          <Divider sx={{ margin: theme.spacing(2) }}/>
          <List
            sx={{
              width: '100%',
              marginTop: theme.spacing(4),
              padding: 0,
            }}
          >
            {menus.map((menu, index) => {
              if (menu.type === MenuType.Title) {
                return renderTitleMenu(menu, index);
              } else if (menu.type === MenuType.Action) {
                return renderActionMenu(menu, index);
              } else if (menu.type !== MenuType.Hidden) {
                return renderMenu(menu, index);
              }
            })}
          </List>
        </Grid>
      </Grid>
    );
  };

export default SideLayoutContainer;
