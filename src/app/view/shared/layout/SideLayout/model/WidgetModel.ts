import { ReactNode } from 'react';


class WidgetModel {
  //
  icon: ReactNode;
  onClickIcon: () => void;

  constructor(icon: ReactNode, onClickIcon: () => void) {
    //
    this.icon = icon;
    this.onClickIcon = onClickIcon;
  }
}

export default WidgetModel;
