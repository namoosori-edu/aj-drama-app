const express = require('express');
const next = require('next');
const { createProxyMiddleware } = require('http-proxy-middleware');
const kollection = require('./kollection.json');

const proxy = {
  // '/api/address-book': {
  //   target: 'http://localhost:9093',
  //   pathRewrite: {'/api/address-book': '/'},
  //   changeOrigin: true,
  //   secure: false,
  // },
  '/api': {
    target: 'http://35.190.235.222',
    pathRewrite: {'/api': '/api'},
    changeOrigin: true,
    secure: false,
  },
};

const port = parseInt(process.env.PORT, 10) || 3500;
const app = next({ dev: true });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();

  Object.keys(proxy).forEach((context) => server.use(createProxyMiddleware(context, proxy[context])));
  server.get(`/${process.env.BASE_PATH}/scheme`, (req, res) => res.json(kollection));
  server.all('*', (req, res) => handle(req, res));

  server.listen(port, () => {
    console.log(`> Ready on port ${port}`);
  })
  .on('error', error => {
    throw error;
  });
});
